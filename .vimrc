" vim:fdm=marker


" Vim configuration file
" Gonzalo Martinez
 

" Basics {{{

  set nocompatible            " Usar Vim
  syntax enable               " Syntax highlighting
  set autowrite               " Guardar antes de hacer make, entre otros
  "set fileformat=unix 
  set number                  " Set number on
  
  filetype plugin indent on   " Automatically detect file types
  set mouse=a                 " Enable mouse

  set shortmess+=filmnrxoOtT  " No welcome message

  set more                              " More prompt 
  "set spell                            " Spell checking on
  set omnifunc=syntaxcomplete#Complete  " Set Omnicomplete feature from Vim 

  set tags=tags;/
  set lazyredraw                   " redraw only when we need to.
  set hidden                       " Hidden buffer when is not open
  set wrap linebreak showbreak=...
  set magic
  map 0 ^                          " Remap VIM 0 to first non-blank character
 
  " Set utf8 as standard encoding and en_US as the standard language
  set encoding=utf8
  
  " Use Unix as the standard file type
  set ffs=unix,dos,mac

  " No annoying sound on errors
  set noerrorbells
  set novisualbell
  set t_vb=
  set tm=500

  " Fast saving
  nmap <leader>w :w!<cr>

  " Sets how many lines of history VIM has to remember
  set history=700

  
"}}}


" Vim UI {{{
 
  set cursorline                   " Highlight current line
  hi cursorline guibg   = \#333333 " Highlight bg color of current line
  hi CursorColumn guibg = \#333333 " Highlight cursor
  
  set ruler                        " Show the ruler
  set showcmd                      " Show command in status line

  set backspace=indent,eol,start   " backspace for dummys
  set linespace=0                  " No extra spaces between rows                            
  set showmatch                    " show matching brackets/parenthesis
  set incsearch                    " find as you type search
  set hlsearch                     " highlight search terms
  set winminheight=0               " windows can be 0 line high
  set ignorecase                   " case insensitive search
  set smartcase                    " case sensitive when uc present

  set wildmenu                                      " show list instead of just completing
  set wildmode=list:longest,full                    " command <Tab> completion, list matches, then longest common part, then all
  set wildignore=*.o,*~,*.pyc,*.obj,*.pyc,*.class   " Ignore compiled files

  set whichwrap=b,s,h,l,<,>,[,]    " backspace and cursor keys wrap to
  set scrolljump=5                 " lines to scroll when cursor leaves screen
  set scrolloff=3                  " minimum lines to keep above and below cursor
  set foldenable                   " auto fold code


"}}}


" Formatting {{{

   set nowrap              " Wrap long lines
   set autoindent          " Indent at the same level of the previous line
   set copyindent          " Copy the previous indentation on autoindenting
   set si                  " Smart indent

   set expandtab           " Tabs are spaces, not tabs
   set smarttab

   set shiftwidth=4        " Use indents of 4 spaces
   set tabstop=4           " An indentation every four columns
   set softtabstop=4       " Let backspace delete indent

   set pastetoggle=<F12>   " Pastetoggle (sane indentation on pastes)


"}}}


" Status Line {{{

   ""let g:Powerline_symbols = 'fancy'
   set laststatus=2                " Always show the status line

   " Format the status line
   "set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l


"}}}


" Key (re)Mappings {{{

     "The default leader is '\\', but many people prefer ',' as it's in a standard
     "location
     let mapleader = ','

     " Making it so ; works like : for commands. Saves typing and eliminates :W style typos due to lazy holding shift.
     nnoremap ; :


     " Easier moving in tabs and windows
     map <C-J> <C-W>j<C-W>_
     map <C-K> <C-W>k<C-W>_
     map <C-L> <C-W>l<C-W>_
     map <C-H> <C-W>h<C-W>_
     map <C-K> <C-W>k<C-W>_

    " Syntastic - toggle error list. Probably should be toggleable.
     noremap <silent><leader>lo :Errors<CR>
     noremap <silent><leader>lc :lcl<CR> 


"}}}


" Files, backups and undo {{{

    " Turn backup off, since most stuff is in SVN, git et.c anyway...
    set nobackup
    set nowb
    set noswapfile
    set directory=$HOME/.vim/swap//  " Same for swap files
    set viewdir=$HOME/.vim/views//   " same for view files


"}}}


" Persistent undo {{{

    set undofile                     " Save undo's after file closes
    set undodir=$HOME/.vim/undo      " Were to save undo histories
    set undolevels=1000              " How many undos
    set undoreload=10000             " Number of lines to save for undo


"}}}


" Plugins{{{

    " Pathogen {{
    
	execute pathogen#infect()
    
     "}}	


    " Numbers {{

    	let g:numbers_exclude=['tagbar', 'gundo', 'minibufexpl', 'nerdtree']

    "}}

	
    " Indent-guides {{

    	let g:indent_guides_enable_on_vim_startup=1
    	let g:indent_guides_start_level=2
    	let g:indent_guides_guide_size=1

    "}}	


    " NERDTree {{

    	autocmd vimenter * if !argc() | NERDTree | endif
    	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

    	let NERDTreeIgnore=['\~$', '\.pyc$', '\.o$', '\.out$', '\.hi$', '\.class$']  " Procesos de compilacion
    	let NERDTreeIgnore+=['\.png$', '\.jpg$', '\.gif$']                           " Imagenes
    	let NERDTreeIgnore+=['\.pid$', '__.py$', 'tags', '\.form$']                  " otros

    	let g:tabular_loaded=1

     "}}	


    " Ctrlp {{

    	let g:ctrlp_map='<c-p>'
    	let g:ctrlp_cmd='CtrlP'
    	let g:ctrlp_working_path_mode='ra'

    	" CtrlP - don't recalculate files on start (slow)
    	let g:ctrlp_clear_cache_on_exit=0
    	let g:ctrlp_custom_ignore='\v[\/]\.(git|hg|svn)$'
    
   	 " Better :sign interface symbols
    	let g:syntastic_error_symbol='✗'
    	let g:syntastic_warning_symbol='!'"


    "}}


    " NERD Commenter settings {{

    	let NERDCreateDefaultMappings=0
    	let NERDCommentWholeLinesInVMode=1
   	let NERDSpaceDelims=1
    	map <leader>c <plug>NERDCommenterToggle

     "}}

    " Super Tab settings {{

    	let g:SuperTabDefaultCompletionType="context"
    	let g:SuperTabMidWordCompletion=0
    
    "}}

    " Unite {{ 

    	let g:unite_source_history_yank_enable=1
    	call unite#filters#matcher_default#use(['matcher_fuzzy'])
    	nnoremap <leader>t :<C-u>Unite -no-split -buffer-name=files   -start-insert file_rec/async:!<cr>
    	nnoremap <leader>f :<C-u>Unite -no-split -buffer-name=files   -start-insert file<cr>
    	nnoremap <leader>r :<C-u>Unite -no-split -buffer-name=mru     -start-insert file_mru<cr>
    	nnoremap <leader>o :<C-u>Unite -no-split -buffer-name=outline -start-insert outline<cr>
    	nnoremap <leader>y :<C-u>Unite -no-split -buffer-name=yank    history/yank<cr>
    	nnoremap <leader>e :<C-u>Unite -no-split -buffer-name=buffer  buffer<cr>

    	" Custom mappings for the unite buffer
    	autocmd FileType unite call s:unite_settings()
    	function! s:unite_settings()

     	" Play nice with supertab
     	let b:SuperTabDisabled=1

      	" Enable navigation with control-j and control-k in insert mode
     	imap <buffer> <C-j>   <Plug>(unite_select_next_line)
     	imap <buffer> <C-k>   <Plug>(unite_select_previous_line)
    	endfunction

    "}}


    " Neocomplcache {{    
    
    	let g:acp_enableAtStartup=0
    	let g:neocomplcache_enable_at_startup=1
    	let g:neocomplcache_enable_smart_case=1
    	let g:neocomplcache_min_syntax_length=2
    	let g:neocomplcache_enable_auto_select=1
    	let g:neocomplcache_max_list=12

    	let g:closetag_html_style=1

    "}}


    " Syntastic {{

    	let g:syntastic_check_on_open=1
    	let g:syntastic_check_on_wq=0   " Don't check on :wq and :x
    	let g:syntastic_enable_signs=1  " Errors on left side
    	let g:syntastic_auto_loc_list=2 " Only show window when I ask
    	let g:syntastic_cpp_check_header=1
    	let g:syntastic_cpp_no_include_search=1
    	let g:syntastic_javascript_checkers=['jshint']
    	let g:syntastic_javascript_jshint_conf='$HOME/.jshintrc'
    	let g:syntastic_html_jshint_conf='$HOME/.jshintrc'
    	let g:syntastic_c_checkers=['gcc']
    	let g:syntastic_cpp_compiler='g++'
    	let g:syntastic_cpp_compiler_options='-std =c++11'
    	let g:syntastic_java_checkers=[]
    	if has('unix')
        	let g:syntastic_error_symbol='★'
        	let g:syntastic_style_error_symbol='>'
        	let g:syntastic_warning_symbol='⚠'
        	let g:syntastic_style_warning_symbol='>'
    	else
        	let g:syntastic_error_symbol='!'
        	let g:syntastic_style_error_symbol='>'
        	let g:syntastic_warning_symbol='.'
        	let g:syntastic_style_warning_symbol='>'
        endif

      "}}


    " Vim-snippets ultisnips {{

    	" Trigger configuration.
    	let g:UltiSnipsExpandTrigger="<tab>"
    	let g:UltiSnipsJumpForwardTrigger="<c-b>"
    	let g:UltiSnipsJumpBackwardTrigger="<c-z>"

    	" If you want :UltiSnipsEdit to split your window.
    	let g:UltiSnipsEditSplit="vertical"

     "}}

     " Vim-clang {{
      
        set completeopt=longest,menuone,preview
     
     "}}

	
"}}}


" Plugins Mappings {{{

     nnoremap <F2> :NERDTree<CR>
     nnoremap <F3> :TagbarToggle<CR>
     nnoremap <F4> :GundoToggle<CR>
     nnoremap <F5> :VimuxPromptCommand<CR>
     nnoremap <F6> :VimuxCloseRunner<CR>

     nnoremap <CR> :nohlsearch<cr>  " clear the search buffer when hitting return

     nnoremap <F7> :Latex <CR>  " Compilar en latex y ejecutar en algun pdfviewver


"}}}


" Latex{{{

    " Compilar en latex y abrir en zathura (zathura es un programa para ver pdfs)
    command Latex execute "silent !pdflatex % > /dev/null && zathura %:r.pdf > /dev/null 2>&1 &" | redraw!


" }}}


" Cierre de parentesis {{{

    inoremap ( ()<Esc>i
    inoremap [ []<Esc>i
    inoremap { {<CR>}<Esc>O
    autocmd Syntax html,vim inoremap < <lt>><Esc>i| inoremap > <c-r>=ClosePair('>')<CR>
    inoremap ) <c-r>=ClosePair(')')<CR>
    inoremap ] <c-r>=ClosePair(']')<CR>
    inoremap } <c-r>=CloseBracket()<CR>
    inoremap " <c-r>=QuoteDelim('"')<CR>
    inoremap ' <c-r>=QuoteDelim("'")<CR>

    function ClosePair(char)
        if getline('.')[col('.') - 1] == a:char
            return "\<Right>"
        else
            return a:char
        endif
    endf

    function CloseBracket()
        if match(getline(line('.') + 1), '\s*}') < 0
            return "\<CR>}"
    else
            return "\<Esc>j0f}a"
        endif
    endf

    function QuoteDelim(char)
        let line = getline('.')
        let col  = col('.')
        if line[col - 2] == "\\"
            "Inserting a quoted quotation mark into the string
            return a:char
        elseif line[col - 1] == a:char
            "Escaping out of the string
            return "\<Right>"
        else
            "Starting a string
            return a:char.a:char."\<Esc>i"
        endif
    endf


"}}}


" Bundle {{{

    set nocompatible              " be iMproved, required
    filetype off                  " required

    " set the runtime path to include Vundle and initialize
    set rtp+=~/.vim/bundle/vundle/
    call vundle#rc()
    " alternatively, pass a path where Vundle should install bundles
    "let path = '~/some/path/here'
    "call vundle#rc(path)

    " let Vundle manage Vundle, required
    Bundle 'gmarik/vundle'

    " The following are examples of different formats supported.
    " Keep bundle commands between here and filetype plugin indent on.
    " scripts on GitHub repos
    Bundle 'tpope/vim-fugitive'
    Bundle 'Lokaltog/vim-easymotion'
    Bundle 'tpope/vim-rails.git'
    " The sparkup vim script is in a subdirectory of this repo called vim.
    " Pass the path to set the runtimepath properly.
    Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
    " scripts from http://vim-scripts.org/vim/scripts.html
    "Bundle 'L9'
   " Bundle 'FuzzyFinder'
    " scripts not on GitHub
    Bundle 'git://git.wincent.com/command-t.git'
    " git repos on your local machine (i.e. when working on your own plugin)
    "Bundle 'file:///home/gmarik/path/to/plugin'
    " ...

    "Snippets
    
    " Vim-snippets

    "Bundle 'MarcWeber/vim-addon-mw-utils'
    "Bundle 'tomtom/tlib_vim'
    "Bundle 'honza/vim-snippets'
    "Bundle 'garbas/vim-snipmate'

    " Track the engine.
    Bundle 'SirVer/ultisnips'

    " Snippets are separated from the engine. Add this if you want them:
    Bundle 'honza/vim-snippets' 

    " Powerline

    Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

    " Git wrapper inside Vim
    Bundle 'tpope/vim-fugitive'

    " Align your = etc.
    Bundle 'vim-scripts/Align'

    " A fancy start screen, shows MRU etc.
    Bundle 'mhinz/vim-startify'

    " Functions, class data etc.
    " REQUIREMENTS: (exuberant)-ctags
    Bundle 'majutsushi/tagbar'

    " Edit files using sudo/su
    Bundle 'chrisbra/SudoEdit.vim'

    " <Tab> everything!
    Bundle 'ervandew/supertab'

    " Fuzzy finder (files, mru, etc)
    Bundle 'kien/ctrlp.vim'

    " A pretty statusline, bufferline integration
    "Bundle 'itchyny/lightline.vim'
    Bundle 'bling/vim-bufferline'

    " NerdCommenter
    " Super easy commenting, toggle comments etc
    Bundle 'scrooloose/nerdcommenter'

    " Emmet plugin 
    Bundle "mattn/emmet-vim"

    " Neocomplcache
    Bundle"Shougo/neocomplcache.vim"

    " Neocomplete plugin
    ""Bundle 'Shougo/neocomplete'

    ""Bundle 'Shougo/neosnippet'
    ""Bundle 'Shougo/neosnippet-snippets'

    " Unite Plugin
    Bundle "Shougo/unite.vim"

    " Vim Shell

    Bundle "Shougo/vimshell.vim"

    " Youcompleteme plugin
    "Bundle 'Valloric/YouCompleteMe'

    "Gundo plugin
    Bundle "sjl/gundo.vim"

    " Vim-easy-align
    Bundle 'junegunn/vim-easy-align'

    Bundle 'terryma/vim-multiple-cursors' 
    
    Bundle 'LaTeX-Box-Team/LaTeX-Box'

    " Vim-dispatch
    Bundle 'tpope/vim-dispatch'

    " vim-clang
    Bundle 'justmao945/vim-clang'

    filetype plugin indent on     " required
    " To ignore plugin indent changes, instead use:
    "filetype plugin on
    "
    " Brief help
    " :BundleList          - list configured bundles
    " :BundleInstall(!)    - install (update) bundles
    " :BundleSearch(!) foo - search (or refresh cache first) for foo
    " :BundleClean(!)      - confirm (or auto-approve) removal of unused bundles
    "
    " see :h vundle for more details or wiki for FAQ
    " NOTE: comments after Bundle commands are not allowed.
    " Put your stuff after this line


"}}}


